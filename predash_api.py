# Integration Flask Tryton with Predash
from werkzeug.exceptions import NotFound
from werkzeug.middleware.dispatcher import DispatcherMiddleware

from factory import create_app
from tools import get_config

config = get_config()
databases = list(eval(config.get('General', 'databases')))
host_ = config.get('General', 'host')
API_KEY = config.get('Auth', 'api_key')
SECRET_KEY = config.get('Auth', 'secret_key')
USER = config.get('Auth', 'user')
active_ssl = False

if 'SSL' in config.sections():
    CERT_FILE = config.get('SSL', 'cert_file')
    KEY_FILE = config.get('SSL', 'key_file')
    if CERT_FILE and KEY_FILE:
        active_ssl = True

manager = {}
for db in databases:
    db_app = create_app(db)
    path = '/' + db
    manager[path] = db_app

app = DispatcherMiddleware(NotFound(), manager)

if __name__ == "__main__":
    from werkzeug.serving import run_simple
    run_simple('0.0.0.0', 5070, app, use_reloader=True, use_debugger=True)

    # print('For testing purpose on standalone database......!')
    # if active_ssl:
    #     db_app.run(host=host_, port=5070, ssl_context=(CERT_FILE, KEY_FILE))
    # else:
    #     db_app.run(host=host_, port=5070)
