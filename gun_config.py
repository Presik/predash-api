import os

default_dir = os.path.join(os.getenv('HOME'), '.certificate')
certificate_ = os.path.join(default_dir, 'fullchain.pem')
keyfile_ = os.path.join(default_dir, 'privkey.pem')

if os.path.exists(certificate_):
    print('Mode: Https Activated')
    certfile = certificate_
    keyfile = keyfile_
else:
    print('Mode: Http')

bind = '0.0.0.0:5070'
proc_name = 'predash_api'
worker_connections = 1000
workers = 2
timeout = 30
keepalive = 2

errorlog = '-'
loglevel = 'info'
accesslog = '-'
access_log_format = '%(h)s %(l)s %(u)s %(t)s "%(r)s" %(s)s %(b)s "%(f)s" "%(a)s"'
